import java.io.FileReader;
import java.io.File;
import java.lang.String;
import java.lang.StringBuffer;
import java.lang.Boolean;
import java.util.Random;
import dist.DiscreteDependencyTree;
import dist.DiscreteUniformDistribution;
import dist.Distribution;
import opt.DiscreteChangeOneNeighbor;
import opt.EvaluationFunction;
import opt.GenericHillClimbingProblem;
import opt.HillClimbingProblem;
import opt.NeighborFunction;
import opt.RandomizedHillClimbing;
import opt.SimulatedAnnealing;
import opt.example.FourPeaksEvaluationFunction;
import opt.ga.CrossoverFunction;
import opt.ga.SingleCrossOver;
import opt.ga.DiscreteChangeOneMutation;
import opt.ga.GenericGeneticAlgorithmProblem;
import opt.ga.GeneticAlgorithmProblem;
import opt.ga.MutationFunction;
import opt.ga.StandardGeneticAlgorithm;
import opt.ga.UniformCrossOver;
import opt.prob.GenericProbabilisticOptimizationProblem;
import opt.prob.MIMIC;
import opt.prob.ProbabilisticOptimizationProblem;
import shared.FixedIterationTrainer;
import opt.example.ContinuousPeaksEvaluationFunction;

public class demo {
    public static void main(String[] args){
        int N = 60;
        int T = N/10;
        int[] ranges = new int[N];
        Arrays.fill(ranges, 2);

        var ef = new ContinuousPeaksEvaluationFunction(T);
        var odd = new DiscreteUniformDistribution(ranges);
        var nf = new DiscreteChangeOneNeighbor(ranges);
        var mf = new DiscreteChangeOneMutation(ranges);
        var cf = new SingleCrossOver();
        var df = new DiscreteDependencyTree(.1, ranges);
        var hcp = new GenericHillClimbingProblem(ef, odd, nf);
        var gap = new GenericGeneticAlgorithmProblem(ef, odd, mf, cf);
        var pop = new GenericProbabilisticOptimizationProblem(ef, odd, df);


        var rhc = new RandomizedHillClimbing(hcp);

        var fit = new FixedIterationTrainer(rhc, 200000);
        fit.train();
        System.out.println("RHC: " + ef.value(rhc.getOptimal()).toString());

    }
}