#!/bin/bash

pushd .. > /dev/null

javac -cp ABAGAIL.jar:. helpers/*.java 
jar cf ProjectHelpers.jar helpers/*.class

popd > /dev/null
