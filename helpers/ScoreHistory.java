package helpers;

public class ScoreHistory{
    public ScoreHistory(int samples){
        iterations = new double[samples];
        scores = new double[samples];
    }
    
    public double[] iterations;
    public double[] scores;
}