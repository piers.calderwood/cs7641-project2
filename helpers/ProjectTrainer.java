package helpers;

import shared.Trainer;
import java.time.Duration;

public class ProjectTrainer implements Trainer {
    
    private int bestIteration;
    private double bestScore;
    private int iterations;
    
    private long startNS;
    private long endNS;
    
    public Trainer trainer;
    
    private int maxIterations;
    
    public ScoreHistory history;
    
    public ProjectTrainer(Trainer trainer, int maxI){
        this.trainer = trainer;
        this.maxIterations = maxI;
        history = new ScoreHistory(maxI/100);
    }
    
    
    public double train() {
        double score = 0;
        startNS = System.nanoTime();
        for(int i = 0; i < maxIterations; i++){
            score = trainer.train();
            if(i%100 == 0){
                history.iterations[i/100] = i+1;
                history.scores[i/100] = score;
            }
            iterations++;
            if(score > bestScore){
                bestIteration = i;
                bestScore = score;
            }
        }
        endNS = System.nanoTime();
        
        return score;
    }
    
    public int getIterations(){
        return iterations;
    }
    
    public int getBestIterations(){
        return bestIteration;
    }
    
    public double getBestScore(){
        return bestScore;
    }
    
    public Duration getDuration(){
        return Duration.ofNanos(endNS - startNS);
    }
}
